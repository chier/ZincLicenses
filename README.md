## Zinc Licenses
![Depolyment](image/logo.png)
> English | [中文](README_CN.md)

### Introduction
Zinc licenses are a collective term for a series of creative protection agreements.  

The main purpose of this series of licenses is to protect creations created through human mental labor from infringement, and at the same time, to open the protected creations to the public to a certain extent.  

This includes but is not limited to code open source protection agreements, literary creation protection agreements, graphic creation protection agreements, etc.  

All Zinc licenses themselves are absolutely open, allowing any user to backup, copy, and distribute them, and all users can make certain modifications according to their own needs.

### Purpose of Issuance
Since the birth of artificial intelligence like ChatGPT, which can create new creations by blending various human creations, human innovation enthusiasm has been severely dampened.  

In order to distinguish human creations from AI creations, and to impose certain restrictions on AI's free use of human creations, this series of licenses was first issued on April 8, 2023 to protect human creations.

### Usage method
The usage method of the Zinc license is similar to that of the software open source agreement, except that its scope of openness and protection is not limited to the software source code, but covers a wider range, and the precautions for use are different.  

You can find the license you need in the [Code-Licenses](Code-Licenses) directory under this warehouse folder.  

Please copy the selected Zinc license file (ending with the .ZINC suffix) to your project folder and open it using a text editor. Replace the [Name] at the end of the file with your or your team's name.